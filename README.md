# Survey Grid Library

## Overview

Lack of free tools in Canada to work with the archaic DLS and NTS systems, led me to compile a semi-complete stand-alone library.

This library also converts UTM to GPS coordinates.

This library is meant to be used to convert DLS and NTS systems to human readable GPS coordinates.

This library is mostly based on Racine Ennis' [work](https://github.com/RaysceneNS/SurveyGridLibrary). For more details of please visit his [blog](https://racineennis.ca/2018/08/31/Nts-grid-coordinate-system.html)

Racine's DLS code covers all of Alberta Township System (ATS), but the database `coordinates.gz` is not extended to the DLS system used in eastern BC. I'm currently using an enhanced version of [this](https://github.com/Walker17/LegalAddressToLatLong) and [this](https://github.com/alephcom/dls/blob/master/generate/dls.cgi) code to approximate the coordinates for these regions.

UTM conversion is based on Sami Salkosuo's [blog](https://www.ibm.com/developerworks/java/library/j-coordconvert/index.html#download) and [Wikipedia](https://en.wikipedia.org/wiki/Meridian_arc#Series_in_terms_of_the_parametric_latitude)

## Install

after cloning the repository:

```bash
cd surverygridlibrary
pip install .
```

## TODO

- Better documentation
- Make better approximation in DLS coordinate conversion
- Batch data conversion
- More Unit Test!!
- Deploy library

## Dependencies

- `numpy`

## Other Resources Used

- https://gis.stackexchange.com/questions/182957/convert-plss-section-township-range-meridian-to-latitude-and-longitude-usin
- https://legallandconverter.com/
