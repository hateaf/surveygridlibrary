#!/usr/bin/env python

import math
from surveygrid.latlong.latlong_coordinate import latlong_coordinate


class nts_system_converter():
    def __init__(self):
        self.blockHeight = 1 / 12
        self.blockWidth = 1 / 8
        self.unitHeight = self.blockHeight / 10
        self.unitWidth = self.blockWidth / 10
        self.quarterHeight = self.unitHeight / 2
        self.quarterWidth = self.unitWidth / 2

        self.latQuarter = {
            'A': 0,
            'B': 0,
            'C': self.quarterHeight,
            'D': self.quarterHeight
        }

        self.lngQuarter = {
            'A': 0,
            'B': self.quarterWidth,
            'C': self.quarterWidth,
            'D': 0
        }

        self.latBlock = {
            'A': 0,
            'B': 0,
            'C': 0,
            'D': 0,
            'E': self.blockHeight,
            'F': self.blockHeight,
            'G': self.blockHeight,
            'H': self.blockHeight,
            'I': self.blockHeight * 2,
            'J': self.blockHeight * 2,
            'K': self.blockHeight * 2,
            'L': self.blockHeight * 2
        }

        self.lngBlock = {
            'A': 0,
            'B': self.blockWidth,
            'C': self.blockWidth * 2,
            'D': self.blockWidth * 3,
            'E': self.blockWidth * 3,
            'F': self.blockWidth * 2,
            'G': self.blockWidth,
            'H': 0,
            'I': 0,
            'J': self.blockWidth,
            'K': self.blockWidth * 2,
            'L': self.blockWidth * 3
        }

        self.latSeries = {
            82: 48,
            83: 52,
            92: 48,
            93: 52,
            94: 56,
            102: 48,
            103: 52,
            104: 56,
            114: 56
        }

        self.lngSeries = {
            82: 112,
            83: 112,
            92: 120,
            93: 120,
            94: 120,
            102: 128,
            103: 128,
            104: 128,
            114: 136
        }

        self.latArea = {
            'A': 0,
            'B': 0,
            'C': 0,
            'D': 0,
            'E': 1,
            'F': 1,
            'G': 1,
            'H': 1,
            'I': 2,
            'J': 2,
            'K': 2,
            'L': 2,
            'M': 3,
            'N': 3,
            'O': 3,
            'P': 3
        }

        self.lngArea = {
            'A': 0,
            'B': 2,
            'C': 4,
            'D': 6,
            'E': 6,
            'F': 4,
            'G': 2,
            'H': 0,
            'I': 0,
            'J': 2,
            'K': 4,
            'L': 6,
            'M': 6,
            'N': 4,
            'O': 2,
            'P': 0
        }

        self.latSheet = {
            1: 0,
            2: 0,
            3: 0,
            4: 0,
            5: 0.25,
            6: 0.25,
            7: 0.25,
            8: 0.25,
            9: 0.5,
            10: 0.5,
            11: 0.5,
            12: 0.5,
            13: 0.75,
            14: 0.75,
            15: 0.75,
            16: 0.75
        }

        self.lngSheet = {
            1: 0,
            2: 0.5,
            3: 1,
            4: 1.5,
            5: 1.5,
            6: 1,
            7: 0.5,
            8: 0,
            9: 0,
            10: 0.5,
            11: 1,
            12: 1.5,
            13: 1.5,
            14: 1,
            15: 0.5,
            16: 0
        }

    def to_latlong(self, nts):
        result = None
        try:
            # The series numbers identify the rectangular areas that have a width of 8 degrees of longitude (width)
            # and 4 degrees of latitude (north south).  The province of British Colombia contains the following series
            # 114|104|094
            # 103|093|083
            # 102|092|082
            latitude = self.latSeries[nts.series]
            longitude = self.lngSeries[nts.series]

            # now refine the coordinate by the map area
            # the map areas divide the series numbers into 16 pieces that are labeled A to P.
            # Each map area is 2 degrees of longitude (width) and 1 degrees of latitude (north south).
            # M|N|O|P
            # L|K|J|I
            # E|F|G|H
            # D|C|B|A
            latitude += self.latArea[nts.area]
            longitude += self.lngArea[nts.area]

            # now refine the coordinate to the map sheet. Each sheet is 0.5 degrees of longitude (width) and
            # 0.25 degrees of latitude (north south)
            # 13|14|15|16
            # 12|11|10|09
            # 05|06|07|08
            # 04|03|02|01
            latitude += self.latSheet[nts.sheet]
            longitude += self.lngSheet[nts.sheet]

            # the map sheet is divided into 12 blocks labeled A-L. Each block is 0.125 degrees of longitude (width) and
            # (0.25/3) degrees of latitude (north south)
            # L|K|J|I
            # E|F|G|H
            # D|C|B|A
            latitude += self.latBlock[nts.block]
            longitude += self.lngBlock[nts.block]

            # the blocks are divided into 100 parts Note the direction of the numbers is always towards the left...
            # 20|19|18|17|16|15|14|13|12|11
            # 10|09|08|07|06|05|04|03|02|01
            y = int(math.ceil((nts.unit - 0.5 - 10) / 10))
            latitude += y * self.unitHeight

            x = nts.unit - y * 10 - 1
            longitude += x * self.unitWidth

            # the units are divided into quarter units A to D
            # C|D
            # B|A
            # NOTE: when evaluating the quarter we add 0.5 to each measurement so that our coordinate
            # is in the center of the quarter rather than on the boundary

            latitude += self.latQuarter[nts.quarter] + self.quarterHeight / 2
            longitude += self.lngQuarter[nts.quarter] + self.quarterWidth / 2

            result = latlong_coordinate(latitude, -longitude)

        except:
            raise ValueError(
                "Error while converting `nts_system` to lat long.")

        return result

    def from_latlong(self, coordinate):
        latitude = math.fabs(coordinate.latitude)
        longitude = math.fabs(coordinate.longitude)

        series = 0
        for key in self.latSeries.keys():
            if latitude >= self.latSeries[key] and latitude < self.latSeries[key] + 4 and \
                    longitude >= self.lngSeries[key] and longitude < self.lngSeries[key] + 8:
                series = key
                break

        if series == 0:
            raise ValueError(
                "The geographic location is not in a BC primary quadrant.")

        latitude -= self.latSeries[series]
        longitude -= self.lngSeries[series]

        area = ""
        for key in self.latArea.keys():
            if latitude >= self.latArea[key] and latitude < self.latArea[key] + 1 and \
                    longitude >= self.lngArea[key] and longitude < self.lngArea[key] + 2:
                area = key
                break

        if area == "":
            raise ValueError("area is invalid.")

        latitude -= self.latArea[area]
        longitude -= self.lngArea[area]

        sheet = 0
        for key in self.latSheet.keys():
            if latitude >= self.latSheet[key] and latitude < self.latSheet[key] + 0.25 and \
                    longitude >= self.lngSheet[key] and longitude < self.lngSheet[key] + 0.5:
                sheet = key
                break

        if sheet == 0:
            raise ValueError("Sheet is invalid.")

        latitude -= self.latSheet[sheet]
        longitude -= self.lngSheet[sheet]

        block = ""
        for key in self.latBlock.keys():
            if latitude >= self.latBlock[key] and latitude < self.latBlock[key] + self.blockHeight and \
                    longitude >= self.lngBlock[key] and longitude < self.lngBlock[key] + self.blockWidth:
                block = key
                break

        if block == "":
            raise ValueError("Block is invalid.")

        latitude -= self.latBlock[block]
        longitude -= self.lngBlock[block]

        y = math.floor(120 * latitude)
        x = math.floor(longitude / 0.0125)
        unit = x + 1 + y * 10

        latitude -= y / 120.0
        longitude -= x * 0.0125

        quarter = ""
        for key in self.lngQuarter.keys():
            if latitude >= self.latQuarter[key] and latitude < self.latQuarter[key] + self.quarterHeight and \
                    longitude >= self.lngQuarter[key] and longitude < self.latQuarter[key] + self.quarterWidth:
                quarter = key
                break

        if quarter == "":
            raise ValueError("Quarter is invalid.")

        ntsString = "{0:1s} {1:3d}-{2:1s}/{3:3d}-{4:1s}-{5:2d}".format(
            quarter,
            unit,
            block,
            series,
            area,
            sheet
        )
        nts = nts_system()
        nts.parse(ntsString)

        return nts


if __name__ == "__main__":
    from nts_system import nts_system
    testcase = "C 007-J/094-G-09"
    nts = nts_system()
    nts.parse(testcase)
    print(nts)

    nts_converter = nts_system_converter()
    coordinate = nts_converter.to_latlong(nts)
    print(coordinate)

    testcase = [57.6729, -122.2094]
    testcoordinate = latlong_coordinate(testcase[0], testcase[1])
    nts_converted = nts_converter.from_latlong(testcoordinate)
    print(nts_converted)
