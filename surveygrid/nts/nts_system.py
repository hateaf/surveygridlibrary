#!/usr/bin/env python

from surveygrid.nts.nts_system_converter import nts_system_converter


def num2string(num, placeholder):
    lenDiff = placeholder - len(str(num))
    if lenDiff < 0:
        placeholder = len(str(num))
        lenDiff = 0
        # print("Warning: placeholder was smaller than the length of the number.")
    return('0'*lenDiff+str(num))


class nts_system():
    '''
    Locations throughout all of Canada can be specified using the National Topographic System (NTS), as it is a system based on lines of latitude and longitude, rather than recorded survey data.
    It is used extensively in British Columbia to mark well and pipeline locations. The exception in BC to this system is the Peace River Block, which is surveyed using the DLS system.
    The NTS system consists of many series (sometimes called maps), identified by series numbers that increase by 1 for the next series to the north, and increase by 10 for the next series
    to the west. Most series are 8 degrees across and 4 degrees high.
    Each series is subdivided into 16 areas, which are given letters from A to P, starting at the southeast corner and then using the same back-and-forth method described for DLS sections above.
    Each area is subdivided into 16 sheets (sometimes called a "map sheet") which are given numbers from 1 to 16.  The back-and-forth method is used for numbering, starting in the southeast corner.
    A sheet is subdivided into 12 blocks, 4 across and 3 high. These are given letters to identify them, from A to L, using the back-and-forth method starting in the southeast corner. Each block is
    subdivided into 100 "units" numbered as 1 to 10, from east to west in the southernmost row, and 11 to 20 from east to west in the next row up, and so on. Finally, each unit is subdivided into 4
    quarter units labeled A, B, C, and D, starting in the southeast and moving clockwise.

    Q-UUU-B/PP-L-SS
    '''

    def __init__(self):

        self.quarter = ""
        self.unit = 0
        self.block = ""
        self.series = 0
        self.area = ""
        self.sheet = 0

        self.num_data = 0
        self.quarters = []
        self.units = []
        self.blocks = []
        self.seriess = []
        self.areas = []
        self.sheets = []

        self.converter = nts_system_converter()

    def _parse_single(self, input, update=True):
        input = input.upper()
        input = input.replace(" ", "")
        quarter = input[0]
        input = input[1:].split("-")

        if quarter < "A" or quarter > "D":
            raise ValueError("Quarter must be in the range A-D.")

        unit = int(input[0])
        if unit < 1 or unit > 100:
            raise ValueError("Unit must be in the range 1-100")

        block, series = input[1].split("/")

        if block < "A" or block > "L":
            raise ValueError("Block must be in the range A-L")

        series = int(series)
        if series < 82 or series > 144:
            raise ValueError("Series must be in the range 82-114")

        area = input[2]
        if area < "A" or area > "P":
            raise ValueError("Map Area must be in the range A-P")

        sheet = int(input[3])
        if sheet < 1 or sheet > 16:
            raise ValueError("Sheet must be in the range 1-16")

        if update:
            self.quarter = quarter
            self.unit = unit
            self. block = block
            self.series = series
            self.area = area
            self.sheet = sheet

        return(quarter, unit, block, series, area, sheet)

    def _parse_array(self, input):
        pass

    def parse(self, input):
        if isinstance(input, list):
            self._parse_array(input)
        else:
            self._parse_single(input)

    def _get_single(self):
        outputString = "{0:1s} {1:3s}-{2:1s}/{3:3s}-{4:1s}-{5:2s}".format(
            self.quarter,
            num2string(self.unit, 3),
            self.block,
            num2string(self.series, 3),
            self.area,
            num2string(self.sheet, 2)
        )
        return outputString

    def _get_array(self):
        return 0

    def get(self):
        if self.num_data == 0:
            return(self._get_single())
        return(self._get_array())

    def to_latlong(self):
        return(self.converter.to_latlong(self))

    def __str__(self):
        if self.num_data == 0:
            return(self._get_single())
        else:
            printArray = self._get_array()
            seperator = "\n"
            return(seperator.join(printArray))


if __name__ == "__main__":
    nts = nts_system()
    testcase = "C 26- f/ 93 -K- 11 "
    nts.parse(testcase)
    print(nts)

    print(nts.to_latlong())
