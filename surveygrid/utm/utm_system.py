#!/usr/bin/env python

import re
from surveygrid.utm.utm_system_converter import utm_system_converter

def num2string(num, placeholder):
    lenDiff = placeholder - len(str(num))
    if lenDiff < 0:
        placeholder = len(str(num))
        lenDiff = 0
        # print("Warning: placeholder was smaller than the length of the number.")
    return('0'*lenDiff+str(num))

class utm_system():
    '''
    The UTM coordinate system is a grid-based method for specifying coordinates. 
    The UTM system divides the Earth into 60 zones, each based on the Transverse 
    Mercator projection. Map projection in cartography is a way to present a 
    two-dimensional curved surface on a plane, such as a normal map.
    '''
    def __init__(self):

        self.zone = 0
        self.hemisphere = ""
        self.northing = 0
        self.easting = 0

        self.num_data = 0
        self.zones = []
        self.hemispheres = []
        self.northings = []
        self.eastings = []

        self.converter = utm_system_converter()

    def _parse_single(self, input, update=True):
        input = input.upper()
        input = input.replace(" ", "")
        input = re.split(r'[;,-]\s*', input)

        if input[0][-1] != "N" and input[0][-1] != "S":
            try:
                int(input[0][-1])
                # print("Assuming Northern Hemisphere.")
                zone = int(input[0])
                hemisphere = "N"
            except ValueError:
                raise ValueError("Hemisphere should be either N or S.")
        else:
            hemisphere = input[0][-1]
            zone = int(input[0][:-1])

        if zone < 1 or zone > 60:
            raise ValueError("Zone should between 1 and 60.")

        if input[1][-1] == "N" and input[2][-1] == "E":
            northing = int(input[1][:-1])
            easting = int(input[2][:-1])
        elif input[1][-1] == "E":
            easting = int(input[1][:-1])
            northing = int(input[2][:-1])
        else:
            raise ValueError("Formatting not correct!\nPlease follow this format: 12N-12324N-12345E")

        if northing < 0 or northing > 10000000:
            raise ValueError("Northing should be between 0 and 10000000.")

        if easting < 0 or easting > 10000000:
            raise ValueError("Easting should be between 0 and 10000000.")

        if update:
            self.zone = zone
            self.hemisphere = hemisphere
            self.northing = northing
            self.easting = easting

        return(zone, hemisphere, northing, easting)

    def _parse_array(self, input):
        pass

    def parse(self, input):
        if isinstance(input, list):
            self._parse_array(input)
        else:
            self._parse_single(input)

    def _get_single(self):
        outputString = "{0:2s}{1:1s}-{2:8s}N-{3:8s}E".format(
            num2string(self.zone, 2),
            self.hemisphere,
            num2string(self.northing, 8),
            num2string(self.easting, 8)
        )
        return outputString

    def _get_array(self):
        return 0

    def get(self):
        if self.num_data == 0:
            return(self._get_single())
        return(self._get_array())

    def to_latlong(self):
        return(self.converter.to_latlong(self))

    def __str__(self):
        if self.num_data == 0:
            return(self._get_single())
        else:
            printArray = self._get_array()
            seperator = "\n"
            return(seperator.join(printArray))


if __name__ == "__main__":
    testcase = "10-571842E; 6280800N"

    utm = utm_system()
    utm.parse(testcase)
    print(utm)