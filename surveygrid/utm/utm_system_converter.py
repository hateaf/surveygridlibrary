#!/usr/bin/env python

import math
from surveygrid.latlong.latlong_coordinate import latlong_coordinate


class utm_system_converter():
    def __init__(self):
        # equatorial radius
        self.equatorialRadius = 6378137
        # polar radius
        self.polarRadius = 6356752.314
        # flattening
        self.flattening = (self.equatorialRadius -
                           self.polarRadius)/self.equatorialRadius
        # inverse flattening
        self.inverseFlattening = 1 / self.flattening
        # mean radius
        self.rm = math.pow(self.equatorialRadius * self.polarRadius, 1 / 2.0)
        self.rm = (self.equatorialRadius + self.polarRadius) / 2
        # scale factor
        self.K0 = 0.9996
        # eccentricity
        self.e = math.sqrt(
            1 - math.pow(self.polarRadius / self.equatorialRadius, 2))

        self.e1sq = self.e * self.e / (1 - self.e * self.e)

        self.n = (self.equatorialRadius - self.polarRadius) / \
            (self.equatorialRadius + self.polarRadius)

        # meridan arc series coefficients
        self.B0 = 1 + (1/4)*math.pow(self.n, 2) + (1/64)*math.pow(self.n, 4)
        self.B2 = -(1/2)*self.n + (1/16)*math.pow(self.n, 3)
        self.B4 = -(1/16)*math.pow(self.n, 2) + (1/64)*math.pow(self.n, 4)
        self.B6 = -(1/48)*math.pow(self.n, 3)
        self.B8 = -(5/512)*math.pow(self.n, 4)

        self.sin1 = 4.84814E-06

    def to_latlong(self, utm):

        zone = utm.zone
        hemisphere = utm.hemisphere
        northing = utm.northing
        easting = utm.easting

        if hemisphere == "S":
            northing = 10000000 - northing

        arc = northing / self.K0
        mu = arc / (self.equatorialRadius * (1 - math.pow(self.e, 2) / 4.0 - 3 *
                                             math.pow(self.e, 4) / 64.0 - 5 * math.pow(self.e, 6) / 256.0))

        ei = (1 - math.pow((1 - self.e * self.e), (1 / 2.0))) / \
            (1 + math.pow((1 - self.e * self.e), (1 / 2.0)))

        ca = 3 * ei / 2 - 27 * math.pow(ei, 3) / 32.0

        cb = 21 * math.pow(ei, 2) / 16 - 55 * math.pow(ei, 4) / 32
        cc = 151 * math.pow(ei, 3) / 96
        cd = 1097 * math.pow(ei, 4) / 512
        phi1 = mu + ca * math.sin(2 * mu) + cb * math.sin(4 * mu) + \
            cc * math.sin(6 * mu) + cd * math.sin(8 * mu)

        n0 = self.equatorialRadius / \
            math.pow((1 - math.pow((self.e * math.sin(phi1)), 2)), (1 / 2.0))

        r0 = self.equatorialRadius * (1 - self.e * self.e) / \
            math.pow((1 - math.pow((self.e * math.sin(phi1)), 2)), (3 / 2.0))
        fact1 = n0 * math.tan(phi1) / r0

        _a1 = 500000 - easting
        dd0 = _a1 / (n0 * self.K0)
        fact2 = dd0 * dd0 / 2

        t0 = math.pow(math.tan(phi1), 2)
        Q0 = self.e1sq * math.pow(math.cos(phi1), 2)
        fact3 = (5 + 3 * t0 + 10 * Q0 - 4 * Q0 * Q0 -
                 9 * self.e1sq) * math.pow(dd0, 4) / 24

        fact4 = (61 + 90 * t0 + 298 * Q0 + 45 * t0 * t0 - 252 *
                 self.e1sq - 3 * Q0 * Q0) * math.pow(dd0, 6) / 720

        lof1 = _a1 / (n0 * self.K0)
        lof2 = (1 + 2 * t0 + Q0) * math.pow(dd0, 3) / 6.0
        lof3 = (5 - 2 * Q0 + 28 * t0 - 3 * math.pow(Q0, 2) + 8 *
                self.e1sq + 24 * math.pow(t0, 2)) * math.pow(dd0, 5) / 120
        _a2 = (lof1 - lof2 + lof3) / math.cos(phi1)
        _a3 = _a2 * 180 / math.pi

        latitude = 180 * (phi1 - fact1 * (fact2 + fact3 + fact4)) / math.pi

        if hemisphere == "S":
            latitude = -latitude

        longitude = ((zone > 0) and (6 * zone - 183.0) or 3.0) - _a3

        return(latlong_coordinate(latitude, longitude))

    def from_latlong(self, coordinate):
        latitude = math.radians(coordinate.latitude)
        longitude = coordinate.longitude

        nu = self.equatorialRadius / \
            math.sqrt(1 - math.pow(self.e * math.sin(latitude), 2))

        if longitude < 0:
            zone = int((180 + longitude) / 6) + 1
        else:
            zone = int(longitude / 6) + 31

        var2 = (6 * zone) - 183
        var3 = longitude - var2
        p = var3 * 3600 / 10000

        S = self.rm * (self.B0 * latitude - self.B2 * math.sin(2 * latitude) + self.B4 *
                       math.sin(4*latitude) - self.B6*math.sin(6 * latitude) + self.B8 * math.sin(8 * latitude) -
                       (2 * self.n * math.sin(2 * latitude)) / (math.sqrt(1 + 2 * self.n * math.cos(2 * latitude) + math.pow(self.n, 2))))

        K1 = self.K0 * S

        K2 = nu * math.sin(latitude) * math.cos(latitude) * \
            math.pow(self.sin1, 2) * self.K0 * 100000000 / 2

        K3 = (math.pow(self.sin1, 4) * nu * math.sin(latitude) * math.pow(math.cos(latitude), 3) / 24) * \
            (5 - math.pow(math.tan(latitude), 2) + 9 * self.e1sq * math.pow(math.cos(latitude), 2) +
             4 * math.pow(self.e1sq, 2) * math.pow(math.cos(latitude), 4)) * self.K0 * 10000000000000000

        K4 = nu * math.cos(latitude) * self.sin1 * self.K0 * 10000

        K5 = math.pow(self.sin1 * math.cos(latitude), 3) * (nu / 6) * (1 - math.pow(
            math.tan(latitude), 2) + self.e1sq * math.pow(math.cos(latitude), 2)) * self.K0 * 1000000000000

        northing = K1 + K2 * p * p + K3 * math.pow(p, 4)
        hemisphere = "N"
        if latitude < 0:
            northing = 10000000 + northing
            hemisphere = "S"

        easting = 500000 + (K4 * p + K5 * math.pow(p, 3))

        utmString = "{0:d}{1:s}-{2:8d}E-{3:8d}N".format(
            zone,
            hemisphere,
            int(easting),
            int(northing)
        )
        utm = utm_system()
        utm.parse(utmString)

        return(utm)


if __name__ == "__main__":

    from utm_system import utm_system
    testcase = "10N-6280800N-571842E"
    utm = utm_system()
    utm.parse(testcase)

    print(utm)

    converter = utm_system_converter()
    coordinates = converter.to_latlong(utm)

    print(coordinates)

    utm_back = converter.from_latlong(coordinates)

    print(utm_back)
