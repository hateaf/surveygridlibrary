#!/usr/bin/env python

from surveygrid.latlong.latlong_coordinate import latlong_coordinate


class latlong_corners():
    def __init__(self, se, sw, nw, ne):

        if not isinstance(se, latlong_coordinate):
            raise ValueError(
                '`se` should be an instance of latlong_coordinate class!')
        if not isinstance(sw, latlong_coordinate):
            raise ValueError(
                '`sw` should be an instance of latlong_coordinate class!')
        if not isinstance(ne, latlong_coordinate):
            raise ValueError(
                '`ne` should be an instance of latlong_coordinate class!')
        if not isinstance(nw, latlong_coordinate):
            raise ValueError(
                '`nw` should be an instance of latlong_coordinate class!')

        self.southEast = se
        self.southWest = sw
        self.northWest = nw
        self.northEast = ne

    def count(self):
        i = 0
        if not self.southEast.isNull:
            i += 1
        if not self.southWest.isNull:
            i += 1
        if not self.northWest.isNull:
            i += 1
        if not self.northEast.isNull:
            i += 1

        return(i)

    def __str__(self):
        printString = '{0:+8.5f}\t| {1:+8.5f}\n{2:+8.5f}\t| {3:+8.5f}\n+==============+|+==============+\n{4:+8.5f}\t| {5:+8.5f}\n{6:+8.5f}\t| {7:+8.5f}\n'.format(
            self.northWest.latitude,
            self.northEast.latitude,
            self.northWest.longitude,
            self.northEast.longitude,
            self.southWest.latitude,
            self.southEast.latitude,
            self.southWest.longitude,
            self.southEast.longitude
        )

        return(printString)


if __name__ == "__main__":
    se = latlong_coordinate(1, -112)
    sw = latlong_coordinate(91, -110)
    nw = latlong_coordinate(92, -111)
    ne = latlong_coordinate(93, 102)
    # print(se)
    test = latlong_corners(se, sw, nw, ne)
    print(test)
