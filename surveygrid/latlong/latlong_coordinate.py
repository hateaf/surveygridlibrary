#!/usr/bin/env python

import math


class latlong_coordinate():
    '''
    Latitude lines are parallel, but longitude lines are not parallel. The distance between two points that 
    are separated by a fixed longitude depends on their latitude. Longitude varies from about 110 km (69 miles)
    per degree at the equator to a few meters (or feet) per degree at the poles. The distance separating one 
    degree of latitude is constant at about 110 km (69 miles)
    To further divide degrees of latitude and longitude seconds and minutes are used:
    1 degree = 60 minutes
    1 minute = 60 seconds
    '''

    def __init__(self, *args, **kwargs):
        '''
        Latitude is specified in degrees within the range [-90, 90]. 
        Longitude is specified in degrees within the range [-180, 180]. Latitude and Longitude values that fall outside the available range are clamped. 
        '''
        self.minLatitude = -90
        self.maxLatitude = 90
        self.minLongitude = -180
        self.maxLongitude = 180
        self.isNull = False

        if len(args) == 0:
            self.isNull = True
            self.latitude = None
            self.longitude = None

        elif len(args) == 2:
            latitude = args[0]
            longitude = args[1]
            if latitude > self.maxLatitude:
                self.latitude = self.maxLatitude
            elif latitude < self.minLatitude:
                self.latitude = self.minLatitude
            else:
                self.latitude = latitude

            if longitude > self.maxLongitude:
                self.longitude = self.maxLongitude
            elif longitude < self.minLongitude:
                self.longitude = self.minLongitude
            else:
                self.longitude = longitude

            # self.longitude = longitude % self.maxLongitude

        elif len(args) == 4:
            latDegrees = args[0]
            latMinutes = args[1]
            longDegrees = args[2]
            longMinutes = args[3]

            if latDegrees > 0:
                self.latitude = latDegrees + latMinutes / 60
            else:
                self.latitude = latDegrees - latMinutes / 60

            if longDegrees > 0:
                self.longitude = longDegrees + longMinutes / 60
            else:
                self.longitude = longDegrees - longMinutes / 60

        elif len(args) == 6:
            latDegrees = args[0]
            latMinutes = args[1]
            latSeconds = args[2]
            longDegrees = args[3]
            longMinutes = args[4]
            longSeconds = args[5]

            if latDegrees > 0:
                self.latitude = latDegrees + \
                    (latMinutes / 60 + latSeconds / 3600)
            else:
                self.latitude = latDegrees - \
                    (latMinutes / 60 + latSeconds / 3600)

            if longDegrees > 0:
                self.longitude = longDegrees + \
                    (longMinutes / 60 + longSeconds / 3600)
            else:
                self.longitude = longDegrees - \
                    (longMinutes / 60 + longSeconds / 3600)

    # TODO: translate other methods from here:
    # https://github.com/RaysceneNS/SurveyGridLibrary/blob/master/SurveyGridLibrary/LatLongCoordinate.cs

    def __eq__(self, other):

        if isinstance(other, latlong_coordinate):
            if not(self.isNull or other.isNull):
                return ((self.latitude == other.latitude) and (self.longitude == other.longitude))
        return False

    def __str__(self):
        printString = ''
        if not self.isNull:
            printString = '{0:7.4f}, {1:7.4f}'.format(
                self.latitude,
                self.longitude
            )
        else:
            printString = 'Null'
        return(printString)

    def relative_distance_to(self, other):
        lat1 = self.latitude
        lat2 = other.latitude
        lon1 = self.longitude
        lon2 = other.longitude

        dLat = math.radians(lat2 - lat1)
        dLon = math.radians(lon2 - lon1)

        s1 = math.sin(dLat / 2)
        s2 = math.sin(dLon / 2)

        a = s1 * s1 + math.cos(math.radians(lat1)) * \
            math.cos(math.radians(lat2)) * s2 * s2
        c = math.atan2(math.sqrt(a), math.sqrt(1 - a))

        return(c)

    def coordinates_from_distance(self, distance, bearing):
        '''
        Given a distance, and bearing, 
        the function will calculate the new coordinates
        '''
        # Radius of the Earth
        earthRadius = 6371

        latitudeRadians = math.radians(self.latitude)
        longitudeRadians = math.radians(self.longitude)

        # calculating new latitude and longitude
        newLatitude = math.asin(
            math.sin(latitudeRadians) *
            math.cos(distance / earthRadius) +
            math.cos(latitudeRadians) *
            math.sin(distance / earthRadius) *
            math.cos(bearing)
        )

        newLongitude = longitudeRadians + math.atan2(
            math.sin(bearing) *
            math.sin(distance / earthRadius) *
            math.cos(latitudeRadians),
            math.cos(distance / earthRadius) -
            math.sin(latitudeRadians) *
            math.sin(latitudeRadians)
        )

        newLatitude = math.degrees(newLatitude)
        newLongitude = math.degrees(newLongitude)

        return(latlong_coordinate(newLatitude, newLongitude))

    def apply_correction(self, latitudeCorrection, longitudeCorrection):
        self.latitude /= latitudeCorrection
        self.longitude /= longitudeCorrection


if __name__ == "__main__":
    testLat = 23
    testLong = 200

    test2Lat = 23
    test2Long = 200

    test = latlong_coordinate(testLat, testLong)
    test2 = latlong_coordinate(test2Lat, test2Long)
    test3 = latlong_coordinate()
    print(test2)
    print(test == test2Lat)
