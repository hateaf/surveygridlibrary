#!/usr/bin/env python

import zlib
import numpy as np
import pkgutil
from surveygrid.latlong.latlong_corners import latlong_corners
from surveygrid.latlong.latlong_coordinate import latlong_coordinate


class dls_coordinate_provider():
    '''
    This is the DLS survey coordinate provider.
    '''

    def __init__(self):
        self._load_data()

    def _load_data(self):
        
        # fileName = "coordinates.gz"
        # with open(fileName, mode='rb') as binfile:
        #     fileContent = binfile.read()

        fileContent = pkgutil.get_data(__name__, "coordinates.gz")

        inflated = zlib.decompress(fileContent, -15)
        inflatedBytes = bytes(inflated)
        self.offset = {}
        buffSize = 1154
        numBuffs = int(len(inflatedBytes) / buffSize)

        for i in range(numBuffs):
            tmp = inflatedBytes[i*buffSize:buffSize*(i+1)]
            key = (tmp[1] << 8 | tmp[0])
            township = np.frombuffer(tmp, dtype='float32', count=288, offset=2)
            self.offset[key] = township

    def township_markers(self, township, rnge, meridian):
        '''
        Returns a dictionary of sections and quarters based on a given township
        '''

        key = meridian << 13 | rnge << 7 | township
        try:
            townshipFloats = self.offset[key]
        except KeyError:
            raise KeyError(
                'Could not find key {0:5d} in `coordinates.gz`'.format(key))

        section = 0
        corners = {}
        for offset in range(0, 288, 8):
            se = self._latlong_coordinate(townshipFloats, offset)
            sw = self._latlong_coordinate(townshipFloats, offset + 2)
            nw = self._latlong_coordinate(townshipFloats, offset + 4)
            ne = self._latlong_coordinate(townshipFloats, offset + 6)

            corners[section] = latlong_corners(se, sw, nw, ne)

            section += 1

        return corners

    def township_boundary(self, township, rnge, meridian):
        '''
        Returns the NW, NE, SW, SE corners of the township

        Asks the boundary provider for a list of sections that border this township
        each township is numbered as:
            31|32|33|34|35|36
            30|29|28|27|26|25
            19|20|21|22|23|24
            18|17|16|15|14|13
            07|08|09|10|11|12
            06|05|04|03|02|01
        looks at each coordinate pair until it finds ones that are the extreme corners
        '''

        key = meridian << 13 | rnge << 7 | township
        try:
            townshipFloats = self.offset[key]
        except KeyError:
            raise KeyError(
                'Could not find key {0:5d} in `coordinates.gz`'.format(key))

        se = latlong_coordinate()
        sw = latlong_coordinate()
        ne = latlong_coordinate()
        nw = latlong_coordinate()

        for i in range(0, 144):
            latitude = townshipFloats[2*i]
            longitude = townshipFloats[2*i + 1]

            if latitude == 0 and longitude == 0:
                continue

            if se.isNull or latitude < se.latitude and longitude > se.longitude:
                se = latlong_coordinate(latitude, longitude)
            if sw.isNull or latitude < sw.latitude and longitude < sw.longitude:
                sw = latlong_coordinate(latitude, longitude)
            if ne.isNull or latitude > ne.latitude and longitude > ne.longitude:
                ne = latlong_coordinate(latitude, longitude)
            if nw.isNull or latitude > nw.latitude and longitude < nw.longitude:
                nw = latlong_coordinate(latitude, longitude)

        return latlong_corners(se, sw, nw, ne)

    def boundary_marker(self, section, township, rnge, meridian):
        '''
        finds the boundary of a section in a township in the binary file
        '''
        key = meridian << 13 | rnge << 7 | township
        try:
            townshipFloats = self.offset[key]
        except KeyError:
            raise KeyError(
                'Could not find key {0:5d} in `coordinates.gz`'.format(key))

        offset = (section - 1) * 8

        se = self._latlong_coordinate(townshipFloats, offset)
        offset += 2

        sw = self._latlong_coordinate(townshipFloats, offset)
        offset += 2

        nw = self._latlong_coordinate(townshipFloats, offset)
        offset += 2

        ne = self._latlong_coordinate(townshipFloats, offset)
        offset += 2

        return latlong_corners(se, sw, nw, ne)

    @staticmethod
    def _latlong_coordinate(townshipFloats, offset):
        latitude = townshipFloats[offset]
        longitude = townshipFloats[offset+1]

        if latitude == 0 and longitude == 0:
            coordinate = None
        else:
            coordinate = latlong_coordinate(latitude, longitude)
        return coordinate


if __name__ == "__main__":
    test = dls_coordinate_provider()
    markers = test.township_markers(1, 1, 1)
    print(markers[22].count())

    corners = test.boundary_marker(1, 1, 1, 1)
    print(corners)
