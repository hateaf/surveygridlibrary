#!/usr/bin/env python

from surveygrid.dls.dls_system_converter import dls_system_converter

def num2string(num, placeholder):
    lenDiff = placeholder - len(str(num))
    if lenDiff < 0:
        placeholder = len(str(num))
        lenDiff = 0
        # print("Warning: placeholder was smaller than the length of the number.")
    return('0'*lenDiff+str(num))


class dls_system():
    '''
    Alberta, Saskatchewan, and parts of Manitoba and parts of British Columbia are mapped on a grid system into townships of 
    approximately 36 square miles (6 mi. x 6mi.).  Each township consists of 36 sections (1 mi. x 1 mi).  
    Each section is further divided into 16 legal subdivisions (LSDs). The numbering system for sections 
    and LSDs uses a back-and-forth system where the numbers may be increasing either to the right or the left
    in the grid. Since the DLS system is based on actual survey data, there can be gaps in the coverage.
    
    Given the location: 04-11-082-04W6
    Legal Sub division	:04
    Section				:11
    Township			:082
    Range				:04
    Meridian			:W6
    '''
    def __init__(self):
        
        self.legalSubdivision = 0
        self.section = 0
        self.township = 0
        self.rnge = 0
        self.meridian = 0
        self.direction = ""
        self.quarter = ""

        self.num_data = 0
        self.legalSubdivisions = []
        self.sections = []
        self.townships = []
        self.ranges = []
        self.meridians = []
        self.directions = []
        self.quarters = []

        self.converter = dls_system_converter()

    def _parse_array(self, inputArray):
        if not isinstance(inputArray, list):
            raise ValueError("Input should be in array form.")

        self.num_data = len(inputArray)
        for _, input in enumerate(inputArray):

            (legalSubdivision, section, township, rnge, meridian,
             quarter, direction) = self._parse_single(input, update=False)
            self.legalSubdivisions.append(legalSubdivision)
            self.sections.append(section)
            self.townships.append(township)
            self.ranges.append(rnge)
            self.meridians.append(meridian)
            self.quarters.append(self.quarter_switch(legalSubdivision))
            self.directions.append(direction)

    def _parse_single(self, input, update=True):
        input = input.upper()
        input = input.replace(" ", "")
        if input[-1] == "M":
            input = input[:-1]

        input = input[:-2]+"-"+input[-2:]
        input = input.split("-")

        try:
            legalSubdivision = int(input[0])
            if legalSubdivision < 1 or legalSubdivision > 16:
                raise ValueError(
                    "Legal sub division must be in the range 1-16")
            quarter = self.quarter_switch(legalSubdivision)
        except ValueError:
            quarter = input[0]

            if quarter == "SE":
                legalSubdivision = 1
            elif quarter == "NE":
                legalSubdivision = 16
            elif quarter == "NW":
                legalSubdivision = 13
            elif quarter == "SW":
                legalSubdivision = 4
            else:
                raise ValueError("Quarter must be either SE, NE, NW or SW.")

        section = int(input[1])
        if section < 1 or section > 36:
            raise ValueError("Section must be in the range 1-36")

        township = int(input[2])
        if township < 1 or township > 127:
            raise ValueError("Township must be in the range 1-127")

        rnge = int(input[3])
        if rnge < 1 or rnge > 34:
            raise ValueError("Range must be in the range 1-34")

        meridian = int(input[4][1:])
        if meridian < 1 or meridian > 6:
            raise ValueError("Meridian must be in the range 1-6")

        direction = input[4][0]
        if direction == "E":
            raise ValueError("East Meridian is not supported.")
        elif direction != "W":
            raise ValueError(
                "DLS location must contain at least one direction as 'W' or 'E'")

        if update:
            self.legalSubdivision = legalSubdivision
            self.section = section
            self.township = township
            self.rnge = rnge
            self.meridian = meridian
            self.quarter = quarter
            self.direction = direction

        return(legalSubdivision, section, township, rnge, meridian, quarter, direction)

    def parse(self, input):
        if isinstance(input, list):
            self._parse_array(input)
        else:
            self._parse_single(input)

    def _get_array(self, format=2):
        outputArray = []
        for i in range(self.num_data):
            outputString = ''
            if format == 1:
                outputString = "{0:2s}-{1:2s}-{2:3s}-{3:2s}-{4:1s}{5:1s}".format(
                    num2string(self.legalSubdivisions[i], 2),
                    num2string(self.sections[i], 2),
                    num2string(self.townships[i], 3),
                    num2string(self.ranges[i], 2),
                    self.directions[i],
                    num2string(self.meridians[i], 1)
                )
            elif format == 2:
                outputString = str(self.legalSubdivisions[i])+"-" + str(self.sections[i])+"-"+str(
                    self.townships[i])+"-"+str(self.ranges[i])+"-"+self.directions[i]+str(self.meridians[i])
            else:
                raise ValueError("format in `get` should be either 1 or 2.")

            outputArray.append(outputString)
        return outputArray

    def _get_single(self, format=2):
        outputString = ''
        if format == 1:
            outputString = "{0:2s}-{1:2s}-{2:3s}-{3:2s}-{4:1s}{5:1s}".format(
                num2string(self.legalSubdivision, 2),
                num2string(self.section, 2),
                num2string(self.township, 3),
                num2string(self.rnge, 2),
                self.direction,
                num2string(self.meridian, 1)
            )
        elif format == 2:
            outputString = str(self.legalSubdivision)+"-" + str(self.section)+"-"+str(
                self.township)+"-"+str(self.rnge)+"-"+self.direction+str(self.meridian)
        else:
            raise ValueError("format in `get` should be either 1 or 2.")
        return outputString

    def get(self, format=2):
        if self.num_data == 0:
            return(self._get_single(format=format))
        return(self._get_array(format=format))

    def to_latlong(self):
        return(self.converter.to_latlong(self))

    def __str__(self):
        if self.num_data == 0:
            return(self._get_single())
        else:
            printArray = self._get_array()
            seperator = "\n"
            return(seperator.join(printArray))

    @staticmethod
    def quarter_switch(legalSubdivision):
        switcher = {
            1: "SE",
            2: "SE",
            3: "SW",
            4: "SW",
            5: "SW",
            6: "SW",
            7: "SE",
            8: "SE",
            9: "NE",
            10: "NE",
            11: "NW",
            12: "NW",
            13: "NW",
            14: "NW",
            15: "NE",
            16: "NE"
        }
        return switcher[legalSubdivision]
