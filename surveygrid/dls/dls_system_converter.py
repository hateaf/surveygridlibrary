#!/usr/bin/env python

from surveygrid.latlong.latlong_coordinate import latlong_coordinate
from surveygrid.dls.dls_coordinate_provider import dls_coordinate_provider
import numpy as np
import math


class dls_system_converter():
    def __init__(self):
        # This is the geodetic height of one section's latitude
        self.sectionHeightInDegrees = 0.014398614

        # This is the geodetic height of one township latitude, note that this is not 6 sections because it includes road allowances.
        # Township lines are numbered from south to north at every 6.0375 miles (9.7164 km), allowing six rows of sections of one mile
        # each between each two township lines, as well as space for three road allowances of 1 chain (approximately 20 meters) each.
        # A total of 127 township lines are defined between latitude 49º North (first baseline) and latitude 60º North (36th baseline).
        self.townshipHeightInDegrees = 0.087300101772

        # This const defines the latitude for the the first baseline, it forms much of the canada usa border.
        self.baseLatitude = 48.99978996

        # The following values specify the longitude values for the 8 basis meridians used in western canada they are
        # the first meridian aka principal or prime meridian through
        # to the coast meridian or 8th.
        self.meridians = [-97.45788889, -102, -106, -
                          110.00506248, -114.00191933, -118.00020192, -122, -122.761]

        self.meridianOrigins = {
            1: latlong_coordinate(self.baseLatitude, self.meridians[0]),
            2: latlong_coordinate(self.baseLatitude, self.meridians[1]),
            3: latlong_coordinate(self.baseLatitude, self.meridians[2]),
            4: latlong_coordinate(self.baseLatitude, self.meridians[3]),
            5: latlong_coordinate(self.baseLatitude, self.meridians[4]),
            6: latlong_coordinate(self.baseLatitude, self.meridians[5]),
            7: latlong_coordinate(self.baseLatitude, self.meridians[6]),
            8: latlong_coordinate(self.baseLatitude, self.meridians[7])
        }

        self.sectionMatrix = np.array([[31, 32, 33, 34, 35, 36],
                                       [30, 29, 28, 27, 26, 25],
                                       [19, 20, 21, 22, 23, 24],
                                       [18, 17, 16, 15, 14, 13],
                                       [7, 8, 9, 10, 11, 12],
                                       [6, 5, 4, 3, 2, 1]], dtype=np.int8)

        self.dls_coordinate_provider_instance = dls_coordinate_provider()

    def accurate_latlong(self, dls):
        '''
        Return `latlong_coordinate` that represents the center of the `dls_system`.
        '''
        dls_boundary = self.dls_coordinate_provider_instance.boundary_marker(
            dls.section, dls.township, dls.rnge, dls.meridian)

        dls_boundary_count = dls_boundary.count()
        if dls_boundary is None or dls_boundary_count == 0:
            raise TypeError(
                "Invalid DLS location for conversion to latitude and longitude.")

        if dls_boundary_count != 4:
            raise ValueError(
                "lookup returned {0:1d} points".format(dls_boundary_count))

        result = self._interpolate_four_points(
            dls.legalSubdivision, dls_boundary)
        return(result)

    def approximate_latlong(self, dls):
        '''
        Roughly approximated the latitude and longitude of a given DLS description.

        Note that the approximation is not good enough for a given LSD.
        '''

        quarter = dls.quarter
        section = dls.section
        township = dls.township
        rnge = dls.rnge
        direction = dls.direction
        meridian = dls.meridian

        # Alberta, Saskatchewan, parts of Manitoba and parts of British Columbia
        # are mapped on a grid system into townships of approximately
        # 36 square miles (6 mi. x 6 mi.)
        townshipDistanceMile = (township - 1) * 6
        rangeDistanceMile = (rnge - 1) * 6

        if quarter == "NW":
            rangeDistanceMile += 0.5
            townshipDistanceMile -= 0.5
        elif quarter == "NE":
            rangeDistanceMile += 0.5
        elif quarter == "SW":
            townshipDistanceMile -= 0.5
        elif quarter == "SE":
            pass
        else:
            raise ValueError("quarter {0:2s} is not valid.".format(quarter))

        # find section and calculate the distance away from the right-bottom
        # corner of the section 1 in the township
        sectionLocation = np.where(self.sectionMatrix == section)
        toTownship = (6 - sectionLocation[1].item()) - 1
        toRange = (6 - sectionLocation[0].item()) - 1

        # Identify the direction to start from the first meridian, and
        # do the calculation
        if direction == "W":
            rangeDistanceMile += toRange
            townshipDistanceMile -= toTownship

            bearing = math.atan2(
                -rangeDistanceMile,
                townshipDistanceMile
            )
        elif direction == "E":
            rangeDistanceMile += toRange
            townshipDistanceMile += toTownship

            bearing = math.atan2(
                rangeDistanceMile,
                townshipDistanceMile
            )
        else:
            raise ValueError(
                "direction {0:1s} is not valid.".format(direction))

        distance = math.sqrt(
            (rangeDistanceMile**2) +
            (townshipDistanceMile**2)
        )
        # converting miles to kilometers
        distance = distance * 1.609344

        result = self.meridianOrigins[meridian].coordinates_from_distance(
            distance, bearing)

        latitudeCorrection, longitudeCorrection = self.approximation_correction(
            township, rnge)
        result.apply_correction(latitudeCorrection, longitudeCorrection)
        return(result)

    def to_latlong(self, dls):
        '''
        try getting the coordinates accurately, otherwise approximate
        '''

        try:
            result = self.accurate_latlong(dls)
        except (KeyError, ValueError):
            # print("Warning: approximate method is being used.")
            result = self.approximate_latlong(dls)

        return(result)

    def from_latlong(self, coordinate):
        inferrance = self.infer_township_for_latlong_coordinates(coordinate)
        if inferrance is None:
            return None
        (inferredMeridian, inferredRange, inferredTownship) = inferrance

        dls_provider = dls_coordinate_provider()

        markers = dls_provider.township_markers(
            inferredTownship, inferredRange, inferredMeridian)
        if markers == {}:
            return None

        # dls = dls_system()
        # dlsString = ""

        bestDistance = 1e20

        for section in range(1, 37):
            dlsBoundary = markers[section - 1]

            if dlsBoundary.count() == 0:
                continue

            for legalSubdivision in range(1, 17):
                if dlsBoundary.count() == 4:
                    testDistance = coordinate.relative_distance_to(
                        self._interpolate_four_points(legalSubdivision, dlsBoundary))

                if testDistance < bestDistance:
                    bestDistance = testDistance
                    dlsString = "{0:3d}-{1:3d}-{2:3d}-{3:3d}W{4:1d}".format(
                        legalSubdivision,
                        section,
                        inferredTownship,
                        inferredRange,
                        inferredMeridian
                    )
        dlsString.replace(" ","")
        return dlsString

    def infer_township_for_latlong_coordinates(self, coordinate):
        longitude = coordinate.longitude
        latitude = coordinate.latitude
        if longitude > self.meridians[0] or longitude < self.meridians[-1]:
            raise ValueError("Meridian is out of range.")

        for k in range(8):
            if longitude <= self.meridians[k] and longitude > self.meridians[k+1]:
                inferredMeridian = k+1
                break

        if inferredMeridian == 0:
            return None

        inferredTownship = math.floor(
            (latitude - self.baseLatitude) / self.townshipHeightInDegrees) + 1

        if inferredTownship <= 0:
            return None

        townshipWidthInDegrees = 6 * \
            self._get_section_width_in_degrees(inferredTownship)

        inferredRange = math.floor(
            (longitude - self.meridians[inferredMeridian - 1]) / townshipWidthInDegrees) + 1

        return(inferredMeridian, inferredRange, inferredTownship)

    def _get_section_width_in_degrees(self, township):
        return self.interpolate(10, -0.02255, 80, -0.026093, township)

    def _interpolate_four_points(self, legalSubdivision, geoList):
        if geoList.southEast.isNull or geoList.southWest.isNull or geoList.northEast.isNull or geoList.northWest.isNull:
            return(latlong_coordinate(0, 0))

        lat = np.zeros((2, 2))
        lng = np.zeros((2, 2))

        lat[0, 0] = geoList.southWest.latitude
        lat[0, 1] = geoList.southEast.latitude
        lat[1, 0] = geoList.northWest.latitude
        lat[1, 1] = geoList.northEast.latitude
        lng[0, 0] = geoList.southWest.longitude
        lng[0, 1] = geoList.southEast.longitude
        lng[1, 0] = geoList.northWest.longitude
        lng[1, 1] = geoList.northEast.longitude

        return(self._bilinear_interpolation(legalSubdivision, lat, lng))

    def _bilinear_interpolation(self, lsd, lat, lng):
        x = [0.875, 0.625, 0.375, 0.125, 0.125, 0.375, 0.625, 0.875,
             0.875, 0.625, 0.375, 0.125, 0.125, 0.375, 0.625, 0.875]
        xp = x[lsd - 1]
        y = [0.125, 0.125, 0.125, 0.125, 0.375, 0.375, 0.375, 0.375,
             0.625, 0.625, 0.625, 0.625, 0.875, 0.875, 0.875, 0.875]
        yp = y[lsd - 1]

        xa = self.interpolate(0, lat[0, 0], 1, lat[1, 0], yp)
        xb = self.interpolate(0, lat[0, 1], 1, lat[1, 1], yp)
        latitude = self.interpolate(0, xa, 1, xb, xp)

        ya = self.interpolate(0, lng[0, 0], 1, lng[0, 1], xp)
        yb = self.interpolate(0, lng[1, 0], 1, lng[1, 1], xp)
        longitude = self.interpolate(0, ya, 1, yb, yp)

        return(latlong_coordinate(latitude, longitude))

    @staticmethod
    def interpolate(x0, y0, x1, y1, z):
        return((z - x1) * y0 / (x0 - x1) + (z - x0) * y1 / (x1 - x0))

    @staticmethod
    def approximation_correction(township, rnge):
        latitudeCorrectionArray = np.array([
            0.999905,
            0.998259,
            0.998851,
            0.996882
        ])
        longitudeCorrectionArray = np.array([
            0.999923,
            0.999201,
            0.999877,
            0.987555
        ])

        latitudeCorrection = (
            latitudeCorrectionArray[0]*(127 - township) * (34 - rnge) +
            latitudeCorrectionArray[1]*(127 - township) * (rnge - 1) +
            latitudeCorrectionArray[2]*(township - 1) * (34 - rnge) +
            latitudeCorrectionArray[3]*(township - 1) * (rnge - 1)
        ) / ((127 - 1) * (34 - 1))

        longitudeCorrection = (
            longitudeCorrectionArray[0]*(127 - township) * (34 - rnge) +
            longitudeCorrectionArray[1]*(127 - township) * (rnge - 1) +
            longitudeCorrectionArray[2]*(township - 1) * (34 - rnge) +
            longitudeCorrectionArray[3]*(township - 1) * (rnge - 1)
        ) / ((127 - 1) * (34 - 1))

        return(latitudeCorrection, longitudeCorrection)


if __name__ == "__main__":
    pass
    # from dls_system import dls_system

    # testcase = "01-01-001-01 W4M"
    # testcase = "15-26-125-5 W1"
    # testcase = "1-1-1-1 W4"

    # test_dls = dls_system()
    # test_dls.parse(testcase)

    # test_dls_converter = dls_system_converter()
    # a = test_dls_converter.accurate_latlong(test_dls)
    # print(a)

    # b = test_dls_converter.approximate_latlong(test_dls)
    # print(b)

    # test_coordinates = [57.499045, -113.178785]  # 1-16-98-20 W4
    # test_latlong = latlong_coordinate(test_coordinates[0], test_coordinates[1])

    # dls = test_dls_converter.from_latlong(test_latlong)
    # print(dls)
    # c = test_dls_converter.to_latlong(test_dls)
    # print(c)
