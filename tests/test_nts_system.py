import unittest


class test_nts_system(unittest.TestCase):
    def test_nts_import(self):
        from surveygrid.nts.nts_system import nts_system

    def test_nts_converter_import(self):
        from surveygrid.nts.nts_system import nts_system_converter


if __name__ == "__main__":
    unittest.main()