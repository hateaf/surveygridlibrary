import unittest


class test_dls_system(unittest.TestCase):
    def test_dls_import(self):
        from surveygrid.dls.dls_system import dls_system

    def test_dls_converter_import(self):
        from surveygrid.dls.dls_system import dls_system_converter

    def test_dls_parse1(self):
        from surveygrid.dls.dls_system import dls_system
        testcase = "02-04-003-02 W4M"
        dls = dls_system()
        dls.parse(testcase)

        self.assertEqual(dls.meridian, 4)
        self.assertEqual(dls.direction, "W")
        self.assertEqual(dls.rnge, 2)
        self.assertEqual(dls.township, 3)
        self.assertEqual(dls.section, 4)
        self.assertEqual(dls.legalSubdivision, 2)
        self.assertEqual(dls.quarter, "SE")

    def test_dls_parse2(self):
        from surveygrid.dls.dls_system import dls_system
        testcase = "NW-04-003-02W4"
        dls = dls_system()
        dls.parse(testcase)

        self.assertEqual(dls.meridian, 4)
        self.assertEqual(dls.direction, "W")
        self.assertEqual(dls.rnge, 2)
        self.assertEqual(dls.township, 3)
        self.assertEqual(dls.section, 4)
        self.assertEqual(dls.legalSubdivision, 13)
        self.assertEqual(dls.quarter, "NW")

    def test_dls_array_parse(self):
        from surveygrid.dls.dls_system import dls_system
        testcase = ["01-01-001-01 W4M", "NE-04-123-02W6"]
        dls = dls_system()
        dls.parse(testcase)

        self.assertEqual(dls.meridians[0], 4)
        self.assertEqual(dls.directions[0], "W")
        self.assertEqual(dls.ranges[0], 1)
        self.assertEqual(dls.townships[0], 1)
        self.assertEqual(dls.sections[0], 1)
        self.assertEqual(dls.legalSubdivisions[0], 1)
        self.assertEqual(dls.quarters[0], "SE")

        self.assertEqual(dls.meridians[1], 6)
        self.assertEqual(dls.directions[1], "W")
        self.assertEqual(dls.ranges[1], 2)
        self.assertEqual(dls.townships[1], 123)
        self.assertEqual(dls.sections[1], 4)
        self.assertEqual(dls.legalSubdivisions[1], 16)
        self.assertEqual(dls.quarters[1], "NE")

    def test_convert_exact(self):
        from surveygrid.dls.dls_system import dls_system
        testcase = "15-26-125-5 W4"
        dls = dls_system()
        dls.parse(testcase)
        real_coordinates = [59.8938, -110.7433]
        coordinates = dls.to_latlong()

        self.assertAlmostEqual(
            real_coordinates[0], coordinates.latitude, delta=0.0001)

        self.assertAlmostEqual(
            real_coordinates[1], coordinates.longitude, delta=0.0001)

    def test_convert_approximate(self):
        from surveygrid.dls.dls_system import dls_system
        testcase = "15-26-125-5 W1"
        dls = dls_system()
        dls.parse(testcase)
        real_coordinates = [59.8330, -98.2661]
        coordinates = dls.to_latlong()

        self.assertAlmostEqual(
            real_coordinates[0], coordinates.latitude, delta=0.0001)

        self.assertAlmostEqual(
            real_coordinates[1], coordinates.longitude, delta=0.0001)

    def test_reverse_convert(self):
        pass


if __name__ == "__main__":
    unittest.main()
