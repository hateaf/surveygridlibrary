from setuptools import setup, find_packages
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(name='surveygridlibrary',
      version='0.1',
      url='https://gitlab.com/hateaf/surveygridlibrary',
      license='GPL',
      author='Hatef Khadivi',
      author_email='hatef.khadivinassab@gmail.com',
      long_description=long_description,
      long_description_content_type='text/markdown',
      description='An open-source library for management of Survey systems used within Canada.',
      packages=find_packages(exclude=['tests']),
      package_data={'surveygrid': ['dls/coordinates.gz']},
      install_requires=['numpy'],
      python_requires='>=3.5, <4',
      test_suite='nose.collector',
      tests_require=['nose', 'numpy'],
      zip_safe=False)